package com.foreverrafs.birthday.di

import android.app.Application
import androidx.room.Room
import com.foreverrafs.birthday.data.local.BirthdayDao
import com.foreverrafs.birthday.data.local.BirthdayDatabase
import com.foreverrafs.birthday.data.network.BirthdayService
import com.foreverrafs.birthday.repository.Repository
import com.foreverrafs.birthday.repository.impl.BirthdayRepository
import com.foreverrafs.birthday.util.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideDispatcher() = Dispatchers.Default

    @Provides
    @Singleton
    fun provideBirthdayRepository(service: BirthdayService, birthdayDao: BirthdayDao): Repository{
        return BirthdayRepository(service, birthdayDao)
    }

    @Provides
    @Singleton
    fun provideBirthdayService(retrofit: Retrofit) : BirthdayService{
        return retrofit.create(BirthdayService::class.java)
    }

    @Singleton
    @Provides
    fun provideBirthdayDatabase(context: Application): BirthdayDatabase {
        return Room
            .databaseBuilder(context, BirthdayDatabase::class.java, "birthday.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideBirthdayDao(database: BirthdayDatabase): BirthdayDao {
        return database.birthdayDao()
    }
}