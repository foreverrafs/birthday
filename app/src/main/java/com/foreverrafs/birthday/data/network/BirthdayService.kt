package com.foreverrafs.birthday.data.network

import com.foreverrafs.birthday.data.model.BirthdayEntity
import retrofit2.http.*

interface BirthdayService {
    @GET("users")
    suspend fun getAllBirthdays(): List<BirthdayEntity>

    @POST("users")
    suspend fun addBirthday(@Body birthdayEntity: BirthdayEntity): BirthdayEntity

    @DELETE("users/{id}")
    suspend fun deleteBirthday(@Path("id") id: Int)
}