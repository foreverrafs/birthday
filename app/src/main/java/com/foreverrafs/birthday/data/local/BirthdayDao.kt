package com.foreverrafs.birthday.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.foreverrafs.birthday.data.model.BirthdayEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface BirthdayDao {
    @Query("SELECT * FROM birthday")
    fun loadFavoriteBirthdays(): Flow<List<BirthdayEntity>>

    @Insert
    fun addFavoriteBirthday(birthday: BirthdayEntity): Long

    @Delete
    fun deleteFavoriteBirthday(birthday: BirthdayEntity): Int
}