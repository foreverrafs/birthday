package com.foreverrafs.birthday.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.foreverrafs.birthday.data.model.BirthdayEntity


@Database(entities = [BirthdayEntity::class], version = 1, exportSchema = false)
abstract class BirthdayDatabase : RoomDatabase() {
    abstract fun birthdayDao(): BirthdayDao
}