package com.foreverrafs.birthday.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "birthday")
data class BirthdayEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("date_of_birth")
    val dateOfBirth: String
)