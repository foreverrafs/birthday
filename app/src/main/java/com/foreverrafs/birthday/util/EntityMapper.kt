package com.foreverrafs.birthday.util

import com.foreverrafs.birthday.data.model.BirthdayEntity
import com.foreverrafs.birthday.domain.model.Birthday
import java.time.LocalDate
import java.time.format.DateTimeFormatter

val networkBirthdayFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")

val BirthdayEntity.toDomainModel: Birthday
    get() = Birthday(
        id = id.toInt(),
        name = name,
        dateOfBirth = LocalDate.from(networkBirthdayFormatter.parse(dateOfBirth))
    )

val List<BirthdayEntity>.toDomainModel: List<Birthday>
    get() = map { it.toDomainModel }


val Birthday.toNetworkEntity: BirthdayEntity
    get() = BirthdayEntity(
        id = id.toString(),
        name = name,
        dateOfBirth = networkBirthdayFormatter.format(dateOfBirth)
    )

val List<BirthdayEntity>.toNetworkEntity: List<Birthday>
    get() = map { it.toDomainModel }
