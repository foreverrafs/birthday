package com.foreverrafs.birthday.util

import android.content.Context
import android.view.View
import com.foreverrafs.birthday.R
import com.foreverrafs.birthday.domain.model.Birthday
import java.time.LocalDate
import java.time.Period
import java.time.temporal.ChronoUnit

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun List<Birthday>.getTodayBirthdays(): List<Birthday> {
    return this.filter {
        it.dateOfBirth == LocalDate.now()
    }
}

fun List<Birthday>.getUpComingBirthdays(): List<Birthday> {

    return this.filter {
        val difference = ChronoUnit.DAYS.between(
            it.dateOfBirth,
            LocalDate.now()
        )

        difference <= 12
    }
}

fun generateUpcomingLabelString(
    context: Context,
    ageLabel: String,
    dateOfBirth: LocalDate
): String {
    val today = LocalDate.now()

    //let's calculate how many months/days to new age
    val remainingPeriod = Period.between(
        today,
        LocalDate.of(
            today.year,
            dateOfBirth.month,
            dateOfBirth.dayOfMonth
        ),
    )

    val remainingString: String =
        context.getString(R.string.days_to_birthday, remainingPeriod.days)


    return "$ageLabel $remainingString"
}