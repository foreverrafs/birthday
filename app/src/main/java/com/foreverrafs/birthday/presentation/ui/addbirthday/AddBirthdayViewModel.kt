package com.foreverrafs.birthday.presentation.ui.addbirthday

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.presentation.state.AddBirthdayUiState
import com.foreverrafs.birthday.repository.Repository
import com.foreverrafs.birthday.util.toNetworkEntity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class AddBirthdayViewModel @ViewModelInject
constructor(
    private val birthdayRepository: Repository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _addBirthdayState = MutableStateFlow<AddBirthdayUiState>(AddBirthdayUiState.Idle)

    val addBirthdayState = _addBirthdayState.asStateFlow()

    fun addBirthday(birthday: Birthday) = viewModelScope.launch(dispatcher) {
        try {
            birthdayRepository.addBirthday(birthday.toNetworkEntity)
            _addBirthdayState.value = AddBirthdayUiState.Saved
        } catch (exception: Throwable) {
            _addBirthdayState.value = AddBirthdayUiState.Error(error = exception)
        }
    }
}
