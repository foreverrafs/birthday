package com.foreverrafs.birthday.presentation.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.presentation.state.HomeUiState
import com.foreverrafs.birthday.repository.Repository
import com.foreverrafs.birthday.util.toDomainModel
import com.foreverrafs.birthday.util.toNetworkEntity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


private const val TAG = "BirthdayViewModel"

class BirthdayViewModel @ViewModelInject
constructor(
    private val birthdayRepository: Repository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    init {
        loadBirthdays()
    }

    private val _homeUiState = MutableStateFlow<HomeUiState>(HomeUiState.Loading)

    val homeUiState = _homeUiState.asStateFlow()

    private fun loadBirthdays() = viewModelScope.launch(dispatcher) {
        birthdayRepository.getAllBirthday()
            .catch { throwable ->
                throwable.cause?.let {
                    _homeUiState.value = HomeUiState.Error(error = it)
                }
            }
            .collect { birthdays ->
                _homeUiState.value = HomeUiState.Birthdays(data = birthdays.toDomainModel)
            }
    }

    fun addBirthdayToFavorites(birthday: Birthday) = viewModelScope.launch(dispatcher) {
        try {
            birthdayRepository.addFavoriteBirthday(birthday.toNetworkEntity)
            _homeUiState.value = HomeUiState.FavoriteAdded
        } catch (e: Exception) {
            _homeUiState.value = HomeUiState.FavoriteError
        }
    }
}