package com.foreverrafs.birthday.presentation.state

sealed class AddBirthdayUiState {
    object Saved : AddBirthdayUiState()
    object Idle : AddBirthdayUiState()
    data class Error(val error: Throwable) : AddBirthdayUiState()
}