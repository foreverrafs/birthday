package com.foreverrafs.birthday.presentation.ui.addbirthday

import com.google.android.material.datepicker.CalendarConstraints
import kotlinx.parcelize.Parcelize


@Parcelize
class BirthdayValidator : CalendarConstraints.DateValidator {
    override fun isValid(date: Long): Boolean = date <= System.currentTimeMillis()
}
