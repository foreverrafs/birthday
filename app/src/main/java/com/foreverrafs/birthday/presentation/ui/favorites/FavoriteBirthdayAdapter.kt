package com.foreverrafs.birthday.presentation.ui.favorites

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.RecyclerView
import com.foreverrafs.birthday.R
import com.foreverrafs.birthday.databinding.ItemFavoriteBinding
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.util.generateUpcomingLabelString
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

class FavoriteBirthdayAdapter(
    private val onDelete: (birthday: Birthday) -> Unit = {},
) :
    RecyclerView.Adapter<FavoriteBirthdayAdapter.FavoriteViewHolder>() {


    private val diffCallback = object : ItemCallback<Birthday>() {
        override fun areItemsTheSame(oldItem: Birthday, newItem: Birthday): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Birthday, newItem: Birthday): Boolean {
            return oldItem == newItem
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallback)


    inner class FavoriteViewHolder(private val binding: ItemFavoriteBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val dateFormatter = DateTimeFormatter.ofPattern("dd MMMM, yyyy")
        private val context = binding.root.context
        private val today = LocalDate.now()

        fun bind(birthday: Birthday) = with(binding) {
            tvDateOfBirth.text = dateFormatter.format(birthday.dateOfBirth)
            tvName.text = birthday.name

            btnDelete.setOnClickListener {
                onDelete(birthday)
            }

            val ageDifference = Period.between(
                birthday.dateOfBirth,
                today
            )

            val ageLabel: String

            //upcoming age so we increment it
            val age = if (ageDifference.years != 0) {
                ageLabel = context.getString(R.string.years)
                ageDifference.years
            } else {
                ageLabel = context.getString(R.string.month)
                ageDifference.months
            } + 1

            tvAge.text = age.toString()
            labelAge.text = generateUpcomingLabelString(context, ageLabel, dateOfBirth = birthday.dateOfBirth)
        }

    }

    fun submitBirthdays(birthdays: List<Birthday>, onSubmitted: () -> Unit = {}) {
        listDiffer.submitList(birthdays) {
            onSubmitted()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {

        val binding = ItemFavoriteBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return FavoriteViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: FavoriteViewHolder, position: Int) {
        viewHolder.bind(listDiffer.currentList[position])
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

}