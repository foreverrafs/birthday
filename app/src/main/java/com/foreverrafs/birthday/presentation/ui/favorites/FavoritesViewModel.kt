package com.foreverrafs.birthday.presentation.ui.favorites

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.presentation.state.FavoritesUiState
import com.foreverrafs.birthday.repository.Repository
import com.foreverrafs.birthday.util.toDomainModel
import com.foreverrafs.birthday.util.toNetworkEntity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


private const val TAG = "FavoritesViewModel"

class FavoritesViewModel @ViewModelInject
constructor(
    private val birthdayRepository: Repository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    init {
        loadFavoriteBirthdays()
    }

    private val _favoritesUiState = MutableStateFlow<FavoritesUiState>(FavoritesUiState.Loading)

    val favoritesUiState = _favoritesUiState.asStateFlow()

    private fun loadFavoriteBirthdays() = viewModelScope.launch(dispatcher) {
        birthdayRepository.getFavoriteBirthdays()
            .catch { throwable ->
                throwable.cause?.let {
                    _favoritesUiState.value = FavoritesUiState.Error(error = it)
                }
            }
            .collect { birthdays ->
                _favoritesUiState.value = FavoritesUiState.Favorites(data = birthdays.toDomainModel)
            }
    }

    fun deleteFavoriteBirthday(favoriteBirthday: Birthday) = viewModelScope.launch(dispatcher) {
        try {
            birthdayRepository.deleteFavoriteBirthday(favoriteBirthday.toNetworkEntity)
            loadFavoriteBirthdays()
        } catch (e: Exception) {
            Log.e(TAG, "deleteFavoriteBirthday: ", e)
        }
    }

}