package com.foreverrafs.birthday.presentation.ui.addbirthday

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.doOnLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.foreverrafs.birthday.R
import com.foreverrafs.birthday.databinding.FragmentAddBirthdayBinding
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.presentation.BaseFragment
import com.foreverrafs.birthday.presentation.state.AddBirthdayUiState
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import kotlin.random.Random

@AndroidEntryPoint
class AddBirthdayFragment : BaseFragment<FragmentAddBirthdayBinding>() {
    private val viewModel: AddBirthdayViewModel by viewModels()

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAddBirthdayBinding {
        return FragmentAddBirthdayBinding.inflate(inflater, container, false)
    }

    private var birthday: LocalDate = LocalDate.now()
    private val dateFormatter = DateTimeFormatter.ofPattern("dd MMMM, yyyy")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeUi()
        observeUiState()
    }

    private fun observeUiState() = lifecycleScope.launchWhenStarted {
        viewModel.addBirthdayState.collect { state ->
            when (state) {
                AddBirthdayUiState.Saved -> {
                    Toast.makeText(requireContext(), "Birthday Saved", Toast.LENGTH_SHORT).show()
                    navController.popBackStack()
                }
                AddBirthdayUiState.Idle -> {
                }
                is AddBirthdayUiState.Error -> {
                    Toast.makeText(
                        requireContext(),
                        "Error Saving Birthday",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun initializeUi() = with(binding) {
        etDateOfBirth.setText(dateFormatter.format(LocalDate.now()))

        etFullName.doOnLayout {
            it.requestFocus()
        }

        etDateOfBirth.setOnClickListener {
            val constraints = CalendarConstraints
                .Builder()
                .setValidator(BirthdayValidator())
                .build()

            val datePicker = MaterialDatePicker.Builder.datePicker()
                .setCalendarConstraints(constraints)
                .build()

            datePicker.addOnPositiveButtonClickListener { date ->
                birthday =
                    Instant.ofEpochMilli(date).atZone(ZoneId.systemDefault()).toLocalDate()

                etDateOfBirth.setText(dateFormatter.format(birthday))
            }

            datePicker.show(childFragmentManager, "")
        }

        btnAddBirthday.setOnClickListener {
            if (etFullName.text?.isNotEmpty()!!) {
                saveBirthday(
                    name = etFullName.text.toString(),
                    date = birthday
                )
            } else {
                MaterialAlertDialogBuilder(requireContext()).setMessage("Name cannot be empty")
                    .setTitle("Input Error")
                    .setPositiveButton(R.string.ok, null)
                    .show()

            }
        }
    }

    private fun saveBirthday(name: String, date: LocalDate) {
        val birthday = Birthday(id = Random.nextInt(), name = name, dateOfBirth = date)

        viewModel.addBirthday(birthday)
    }
}