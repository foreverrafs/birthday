package com.foreverrafs.birthday.presentation.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.RecyclerView
import com.foreverrafs.birthday.R
import com.foreverrafs.birthday.databinding.ItemBirthdayBinding
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.util.generateUpcomingLabelString
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

class BirthdayAdapter(
    private val birthdayViewType: BirthdayViewType,
    private val onFavoriteClicked: (birthday: Birthday) -> Unit = {},
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val diffCallback = object : ItemCallback<Birthday>() {
        override fun areItemsTheSame(oldItem: Birthday, newItem: Birthday): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Birthday, newItem: Birthday): Boolean {
            return oldItem == newItem
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallback)


    inner class TodayBirthdayViewHolder(private val binding: ItemBirthdayBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val dateFormatter = DateTimeFormatter.ofPattern("dd MMMM, yyyy")
        private val context = binding.root.context

        fun bind(birthday: Birthday) = with(binding) {
            tvDateOfBirth.text = dateFormatter.format(birthday.dateOfBirth)
            tvName.text = birthday.name


            btnFavorite.setOnClickListener {
                onFavoriteClicked(birthday)
            }


            val ageDifference = Period.between(
                birthday.dateOfBirth,
                LocalDate.now()
            )

            val ageLabel: String

            val age = if (ageDifference.years != 0) {
                ageLabel = context.getString(R.string.years_old_today)
                ageDifference.years
            } else {
                ageLabel = context.getString(R.string.months_old_today)
                ageDifference.months
            }

            tvAge.text = age.toString()
            labelAge.text = ageLabel
        }
    }

    inner class UpcomingViewHolder(private val binding: ItemBirthdayBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val dateFormatter = DateTimeFormatter.ofPattern("dd MMMM, yyyy")
        private val context = binding.root.context
        private val today = LocalDate.now()

        fun bind(birthday: Birthday) = with(binding) {
            tvDateOfBirth.text = dateFormatter.format(birthday.dateOfBirth)
            tvName.text = birthday.name

            btnFavorite.setOnClickListener {
                onFavoriteClicked(birthday)
            }

            val ageDifference = Period.between(
                birthday.dateOfBirth,
                today
            )

            val ageLabel: String

            //upcoming age so we increment it
            val age = if (ageDifference.years != 0) {
                ageLabel = context.getString(R.string.years)
                ageDifference.years
            } else {
                ageLabel = context.getString(R.string.month)
                ageDifference.months
            } + 1

            tvAge.text = age.toString()
            labelAge.text =
                generateUpcomingLabelString(context, ageLabel, dateOfBirth = birthday.dateOfBirth)
        }

    }

    fun submitBirthdays(birthdays: List<Birthday>, onSubmitted: () -> Unit = {}) {
        listDiffer.submitList(birthdays) {
            onSubmitted()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (birthdayViewType) {
            BirthdayViewType.TODAY -> {
                val binding = ItemBirthdayBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )

                TodayBirthdayViewHolder(binding)
            }
            BirthdayViewType.UPCOMING -> {
                val binding = ItemBirthdayBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )

                UpcomingViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (viewHolder) {
            is TodayBirthdayViewHolder -> viewHolder.bind(listDiffer.currentList[position])
            is UpcomingViewHolder -> viewHolder.bind(listDiffer.currentList[position])
        }
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

    enum class BirthdayViewType {
        TODAY,
        UPCOMING
    }
}