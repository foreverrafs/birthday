package com.foreverrafs.birthday.presentation.state

import com.foreverrafs.birthday.domain.model.Birthday

sealed class HomeUiState {
    object Loading : HomeUiState()
    data class Birthdays(val data: List<Birthday>) : HomeUiState()
    object Empty : HomeUiState()
    data class Error(val error: Throwable) : HomeUiState()
    object FavoriteAdded : HomeUiState()
    object FavoriteError: HomeUiState()
}