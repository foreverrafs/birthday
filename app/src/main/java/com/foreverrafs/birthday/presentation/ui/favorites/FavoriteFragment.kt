package com.foreverrafs.birthday.presentation.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.foreverrafs.birthday.databinding.FragmentFavoriteBinding
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.presentation.BaseFragment
import com.foreverrafs.birthday.presentation.state.FavoritesUiState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


private const val TAG = "FavoriteFragment"

@AndroidEntryPoint
class FavoriteFragment : BaseFragment<FragmentFavoriteBinding>() {
    private val viewModel: FavoritesViewModel by viewModels()
    private val favoriteAdapter = FavoriteBirthdayAdapter(onDelete = ::onFavoriteBirthdayDeleted)


    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentFavoriteBinding {
        return FragmentFavoriteBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpFavoriteList()
        observeUiState()
    }

    private fun setUpFavoriteList(){
        binding.listFavoriteBirthdays.adapter = favoriteAdapter
    }

    private fun onFavoriteBirthdayDeleted(favoriteBirthday: Birthday){
        viewModel.deleteFavoriteBirthday(favoriteBirthday)
    }

    private fun observeUiState() = lifecycleScope.launchWhenStarted {
        viewModel.favoritesUiState.collect { state ->
            when(state){
                FavoritesUiState.Loading -> {}
                is FavoritesUiState.Favorites -> renderFavoriteList(state.data)
                FavoritesUiState.Empty -> {}
                is FavoritesUiState.Error -> {}
            }
        }
    }

    private fun renderFavoriteList(favoriteBirthdays: List<Birthday>) {
        favoriteAdapter.submitBirthdays(favoriteBirthdays)
    }
}