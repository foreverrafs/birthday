package com.foreverrafs.birthday.presentation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.foreverrafs.birthday.databinding.FragmentHomeBinding
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.presentation.BaseFragment
import com.foreverrafs.birthday.presentation.state.HomeUiState
import com.foreverrafs.birthday.util.getTodayBirthdays
import com.foreverrafs.birthday.util.getUpComingBirthdays
import com.foreverrafs.birthday.util.invisible
import com.foreverrafs.birthday.util.visible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    private val viewModel: BirthdayViewModel by viewModels()

    private val todayBirthdayAdapter =
        BirthdayAdapter(
            birthdayViewType = BirthdayAdapter.BirthdayViewType.TODAY,
            onFavoriteClicked = ::onFavoriteClicked
        )

    private val upComingBirthdayAdapter =
        BirthdayAdapter(
            birthdayViewType = BirthdayAdapter.BirthdayViewType.UPCOMING,
            onFavoriteClicked = ::onFavoriteClicked
        )

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAddBirthday.setOnClickListener {
            navController.navigate(HomeFragmentDirections.actionHomeToAddBirthday())
        }

        observeHomeUiState()
        setUpBirthdayLists()
    }

    private fun setUpBirthdayLists() = with(binding) {
        listBirthdaysToday.adapter = todayBirthdayAdapter
        listUpcomingBirthdays.adapter = upComingBirthdayAdapter
    }

    private fun onFavoriteClicked(birthday: Birthday) {
        viewModel.addBirthdayToFavorites(birthday)
    }

    private fun observeHomeUiState() = lifecycleScope.launchWhenStarted {
        viewModel.homeUiState.collect { state ->
            when (state) {
                HomeUiState.Loading -> {
                    renderLoadingState()
                }
                is HomeUiState.Birthdays -> {
                    renderBirthdays(state.data)
                }
                HomeUiState.Empty -> renderEmptyState()
                is HomeUiState.Error -> renderErrorState()
                HomeUiState.FavoriteAdded -> {
                    Toast.makeText(requireContext(), "Birthday Favorited", Toast.LENGTH_SHORT)
                        .show()
                }
                HomeUiState.FavoriteError -> {
                    Toast.makeText(requireContext(), "Already Added", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun renderLoadingState() {
        binding.loadingView.visible()
    }

    private fun renderEmptyState() {
        binding.loadingView.invisible()
    }

    private fun renderBirthdays(birthdays: List<Birthday>) {
        with(binding) {
            loadingView.invisible()
            contentView.visible()

            renderTodayBirthdays(birthdays.getTodayBirthdays())
            renderUpcomingBirthdays(birthdays.getUpComingBirthdays())
        }
    }

    private fun renderUpcomingBirthdays(upcomingBirthdays: List<Birthday>) = with(binding) {
        if (upcomingBirthdays.isEmpty()) {
            tvNoUpcomingBirthday.visible()
        } else {
            upComingBirthdayAdapter.submitBirthdays(upcomingBirthdays)
        }
    }

    private fun renderTodayBirthdays(todayBirthdays: List<Birthday>) = with(binding) {
        if (todayBirthdays.isEmpty()) {
            tvNoBirthdayToday.visible()
        } else {
            todayBirthdayAdapter.submitBirthdays(todayBirthdays)
        }
    }

    private fun renderErrorState() {
        binding.loadingView.invisible()
    }
}