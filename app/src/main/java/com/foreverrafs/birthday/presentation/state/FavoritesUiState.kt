package com.foreverrafs.birthday.presentation.state

import com.foreverrafs.birthday.domain.model.Birthday

sealed class FavoritesUiState {
    object Loading : FavoritesUiState()
    data class Favorites(val data: List<Birthday>) : FavoritesUiState()
    object Empty : FavoritesUiState()
    data class Error(val error: Throwable) : FavoritesUiState()
}