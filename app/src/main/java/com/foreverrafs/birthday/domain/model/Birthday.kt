package com.foreverrafs.birthday.domain.model

import java.time.LocalDate

data class Birthday(
    val id: Int,
    val name: String,
    val dateOfBirth: LocalDate,
    val isFavorite: Boolean = false
)