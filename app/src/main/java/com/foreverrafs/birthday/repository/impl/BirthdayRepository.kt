package com.foreverrafs.birthday.repository.impl

import com.foreverrafs.birthday.data.local.BirthdayDao
import com.foreverrafs.birthday.data.model.BirthdayEntity
import com.foreverrafs.birthday.data.network.BirthdayService
import com.foreverrafs.birthday.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class BirthdayRepository(
    private val birthdayService: BirthdayService,
    private val birthdayDao: BirthdayDao
) : Repository {
    override suspend fun getAllBirthday(): Flow<List<BirthdayEntity>> {
        return flowOf(
            birthdayService.getAllBirthdays()
        )
    }

    override suspend fun getFavoriteBirthdays(): Flow<List<BirthdayEntity>> {
        return birthdayDao.loadFavoriteBirthdays()
    }

    override suspend fun addFavoriteBirthday(birthdayEntity: BirthdayEntity): Long {
        return birthdayDao.addFavoriteBirthday(birthdayEntity)
    }

    override suspend fun deleteFavoriteBirthday(birthdayEntity: BirthdayEntity): Int {
        return birthdayDao.deleteFavoriteBirthday(birthdayEntity)
    }

    override suspend fun addBirthday(birthdayEntity: BirthdayEntity): BirthdayEntity {
        return birthdayService.addBirthday(birthdayEntity)
    }
}