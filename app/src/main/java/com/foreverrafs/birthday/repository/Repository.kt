package com.foreverrafs.birthday.repository

import com.foreverrafs.birthday.data.model.BirthdayEntity
import kotlinx.coroutines.flow.Flow

interface Repository {
    suspend fun getAllBirthday() : Flow<List<BirthdayEntity>>
    suspend fun getFavoriteBirthdays() : Flow<List<BirthdayEntity>>
    suspend fun addFavoriteBirthday(birthdayEntity: BirthdayEntity) : Long
    suspend fun deleteFavoriteBirthday(birthdayEntity: BirthdayEntity): Int
    suspend fun addBirthday(birthdayEntity: BirthdayEntity): BirthdayEntity
}