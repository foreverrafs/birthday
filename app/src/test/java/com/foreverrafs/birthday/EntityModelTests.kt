package com.foreverrafs.birthday

import com.foreverrafs.birthday.data.model.BirthdayEntity
import com.foreverrafs.birthday.domain.model.Birthday
import com.foreverrafs.birthday.util.toDomainModel
import com.foreverrafs.birthday.util.toNetworkEntity
import com.google.common.truth.Truth
import org.junit.Test
import java.time.LocalDate


class EntityModelTests {
    @Test
    fun `should convert an entity model to a domain model`() {
        val id = "23"
        val name = "John Doe"

        val entityBirthday = BirthdayEntity(id = id, name = name, dateOfBirth = "24-10-1942")
        val domainBirthday =
            Birthday(id = id.toInt(), name = name, dateOfBirth = LocalDate.of(1942, 10, 24))

        Truth.assertThat(domainBirthday).isEqualTo(entityBirthday.toDomainModel)

    }

    @Test
    fun `should convert a domain model to a an entity model`() {
        val id = "23"
        val name = "John Doe"

        val entityBirthday = BirthdayEntity(id = id, name = name, dateOfBirth = "24-10-1942")
        val domainBirthday =
            Birthday(id = id.toInt(), name = name, dateOfBirth = LocalDate.of(1942, 10, 24))

        Truth.assertThat(entityBirthday).isEqualTo(domainBirthday.toNetworkEntity)

    }
}