## Birthday App Challenge
This is a working implementation to the birthday app challenge from https://github.com/teamchalkboard/chalkboard-coding-challenges/blob/master/birthdays.md

#### Features
 - List today's birthdays
 - List upcoming birthdays
 - Add birthday to favorites to make it accessible from the favorites screen
 - Add a new birthday
 - Validate birthday input
 - Modern Android Development practices with emphasis on separation of concerns

#### Installing and Running 
Clone the repository and build from Android Studio

#### Some Technical Choices and libraries 

 - **Coroutines**: Coroutines in Kotlin provide operations and mechanisms for performing asynchronous calls without the need for callback methods. Coroutines also have a structure for performing structured concurrency which prevents memory leaks in Android. 
 - **Room Persistence**: The android Room Persistence Library is an **ORM** which makes on-device data storage and manipulation easier. It is built on top of the highly popular **SQLite**  database.
 - **Dagger Hilt**:  Dagger Hilt is a very robust and Google Maintained Dependency Injection library optimized for the android platform. This makes it easier to maintain and manage the dependencies required by the components in the android application. It makes the codebase more readable and maintainable. 

#### Observed Issues (Backend)

 - The API at https://my-json-server.typicode.com/teamchalkboard/chalkboard-coding-challenges/users returns `204` created response when a new birthday is added (`POST`) but a subsequent `GET` request returns the original data excluding the newly added one
 - The API at https://my-json-server.typicode.com/teamchalkboard/chalkboard-coding-challenges/users returns a `200` success response when  a `DELETE` command is issued but a subsequent `GET` request returns the original data including the recently deleted data.
 - The data returned from the API at https://my-json-server.typicode.com/teamchalkboard/chalkboard-coding-challenges/users as at today, the 29th of November has no record that falls within the scope of `today's` or `upcoming (in 2 weeks time)` birthdays making the home screen a bit dull. 

#### What I might do differently with time

 - Even though there is good separation of concerns in the codebase, I
   would have wished to use a `clean architecture` for the development.
   This is an extensive architecture which requires a lot of initial
   setup in Android but the setup time pays off when working on a large
   codebase. Clean architecture in Android enforces all of the `SOLID`
   principles to make the codebase high quality and easily maintainable.
   
 - Even though the UI is clean, I could have designed a much more cleaner and more beautiful UI with Material Animations given time. 

#### Contact Details

 - GitHub: https://github.com/rafsanjani
 - LinkedIn: https://www.linkedin.com/in/foreverrafs/
 - Website: https://foreverrafs.com/ 
